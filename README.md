**FAIR LICENSE**

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# jnipcap - Java libpcap wrapper library

This is a [libpcap][1] wrapper library for Java written in Java and c. It is intended as a pure wrapper library, that is, it wraps the functionality of the native libpcap and presents it in a Java-like format. It is designed with the intention to provide developers familiar with libpcap with an API they know and like. Basically, you can read the pcap programming tutorials on [tcpdump][2] and apply them on jnipcap with only few changes which are very natural for Java developers.

A major reason for jnipcap is familiarity and extensibility. Jnipcap is small, easy to use and easy to extend. Protocols and protocol handlers can easily be written in Java and simply called from jnipcap. It's especially useful when doing simple packet process (statistics) or when working or researching custom / proprietary protocols. 

There are other libpcap wrappers available for Java. However, none of them fitted our needs.

* [jNetPcap][3]

	This is a really mighty network library and API. Probably suitable for most people and use cases. However, its size and complexity is its major flaw. It has a rather steep learning curve, especially when you want to extend it with custom protocols. Also, I didn't manage to compile it under all my target OS. Nevertheless, jnipcap is highly influenced by jNetPcaps native bindings.

* [Jpcap][4]

	One of two wrappers with the same name. This hasn't been updated in years and only supports a small number of standard protocols. Unfortunately protocol decoding is done natively. So if you want to add new protocol you have to implement them in c and change the native protocol handler code. So why work with Java anyway?

* [jpcap][5]

	Another very sophisticated network library. Provides visualizations, a shell, an API for packet capture applications and more. So if you need this kind of functionality I recommend this one. Again, I think for quick researching custom / proprietary protocols this one is overloaded.

In summary, jnipcap does not provide much functionality or features. It doesn't come with a protocol library or protocol dissectors / decoders. It has no GUI, CLI, shell, visualizations. All it does, is exposing the native libpcap interface to Java space. Nothing more, nothing less. And that's exactly what I wanted.

## Status

This is work in progress. Packet capturing offline and live works but not all libpcap functions are implemented.

## References

[1]: http://www.tcpdump.org "libpcap"
[2]: http://www.tcpdump.org "tcpdump"
[3]: http://jnetpcap.com/ "jnetpcap"
[4]: http://netresearch.ics.uci.edu/kfujii/Jpcap/doc/ "Jpcap"
[5]: http://jpcap.sourceforge.net/ "jpcap"
