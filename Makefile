PROJECT=jnipcap
VERSION=0.1
SRC = src
JSRC = ${SRC}/main/java
TSRC = ${SRC}/test/java
CSRC = ${SRC}/main/native
TEST = test
BIN = target
JBIN = ${BIN}/classes
TBIN = ${BIN}/test-classes
LIB = include
INC = ${BIN}/include
SYM = Pcap
JUNIT = junit-4.9b4.jar
PACKAGE = de/unikn/disy/net
JPACKAGE = de.unikn.disy.net
TESTPKG = de/unikn/disy/net

LIBS = pcap
JAVA_DIR = $(JAVA_HOME)
CC = gcc

PLATFORM = $(shell "uname")
ifeq ($(PLATFORM), Linux)
	JNI_INCLUDE = $(JAVA_DIR)/include/linux
	OPT = -shared -L.
	SUFFIX = so
else
ifeq ($(PLATFORM), FreeBSD)
	JNI_INCLUDE = $(JAVA_DIR)/include/freebsd
	OPT = -shared -L.
	SUFFIX = so
else
ifeq ($(PLATFORM), SunOS)
	JNI_INCLUDE = $(JAVA_DIR)/include/solaris
	OPT = -G
	SUFFIX = so
else
ifeq ($(PLATFORM), Darwin)
	JNI_INCLUDE = /System/Library/Frameworks/JavaVM.framework/Headers 
	#OPT = -bundle -framework JavaVM
	OPT = -dynamiclib
	SUFFIX = jnilib
else
	CC = C:\MinGW\bin\mingw32-gcc
	OPT = 
	SUFFIX = dll
endif
endif
endif
endif


.phony: class header jnilib

all: junit

junit: jnilib
	#sudo java -cp ${BIN}/ disy.jnipcap.Test
	sudo java -Djava.library.path=${BIN} -cp ${JBIN}:${TBIN}:${LIB}/${JUNIT} org.junit.runner.JUnitCore disy.jnipcap.PcapTest

jnilib: header 
	#${CC} ${OPT} -I ${JNI_INCLUDE} -I ${LIB}/${PROJECT} -l ${LIBS} ${CSRC}/*.c -o ${BIN}/lib${PROJECT}.${SUFFIX}
	${CC} ${OPT} -I ${JNI_INCLUDE} -I ${INC} -l ${LIBS} ${CSRC}/*.c -o ${BIN}/lib${PROJECT}.${SUFFIX}
	#sudo cp ${LIB}/libjnipcap.jnilib /usr/lib/java/

header: class
	#javah -jni -o ${LIB}/${PROJECT}/jnipcap.h -classpath ${JBIN} ${JPACKAGE}.Pcap
	#javah -jni -o ${LIB}/${PROJECT}/jnipcappacketheader.h -classpath ${JBIN} ${JPACKAGE}.PcapPacketHeader
	#javah -jni -o ${INC}/jnipcap.h -classpath ${JBIN} ${JPACKAGE}.Pcap
	#javah -jni -o ${INC}/jnipcappacketheader.h -classpath ${JBIN} ${JPACKAGE}.PcapPacketHeader
	javah -jni -o ${INC}/jnipcap.h -classpath ${JBIN} ${JPACKAGE}.Pcap ${JPACKAGE}.PcapPacketHeader

class: dirs
	javac ${JSRC}/${PACKAGE}/*.java -d ${JBIN}
	#javac -cp ${JBIN}:${LIB}/${JUNIT} ${TSRC}/${PACKAGE}/*.java -d ${TBIN}

dirs:
	mkdir -p ${JBIN}
	mkdir -p ${TBIN}
	#mkdir -p ${LIB}/${PROJECT}
	mkdir -p ${INC}

antmake:
	${CC} ${OPT} -I ${JNI_INCLUDE} -I ${INC} -l ${LIBS} ${CSRC}/*.c -o ${BIN}/lib${PROJECT}.${SUFFIX}

clean:
	rm -rf ${BIN}
	rm -rf ${LIB}
	#sudo rm /usr/lib/java/libjnipcap.jnilib
