/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
package de.unikn.disy.net;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unikn.disy.net.Pcap;
import de.unikn.disy.net.PcapHandler;
import de.unikn.disy.net.PcapPacketHeader;

/**
 *
 * @author zink
 */
public class PcapTest {

	private static class PacketCounter implements PcapHandler<String> {
		private int count = 0;
		public final int count() { return count; }
		public void receive (final String user, final PcapPacketHeader header, final ByteBuffer bytes) {
			count++;
			System.out.print("'");
		}
	}

	public static class PacketPrinter implements PcapHandler<String> {
		private static int count = 0;
		public final int count() { return count; }
		public void receive (final String user, final PcapPacketHeader header, final ByteBuffer bytes) {
			System.out.printf("%03d\n", count++);
			System.out.printf("sec: '%s' usec: '%s' caplen: '%s' len '%s'\n", header.sec(), header.usec(), header.caplen(), header.len());
			while (bytes.remaining() > 0)
				System.out.printf("%02x ", (byte)bytes.get());
			System.out.printf("\n");
		}
	}

	public PcapTest() {
	}

	private StringBuilder errbuf = null;
	private String dev = null;
	private Integer net, mask = null;
	private long ptr = 0L;
	private final int snaplen = 100;
	private final int promisc = 1;
	private final int rfmon = 1;
	private final int toms = 100;

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
		errbuf = new StringBuilder();
		dev = Pcap.lookupdev(errbuf);
		net = new Integer(0);
		mask = new Integer(0);
		ptr = Pcap.create(dev, errbuf);
	}

	@After
	public void tearDown() {
		errbuf = null;
		dev = null;
		net = null;
		mask = null;
		Pcap.close(ptr);
		ptr = 0L;
	}

	/**
	 * Test of lookupdev method, of class Pcap.
	 */
	@Test
	public void testLookupdev() {
		System.out.println("lookupdev");
		final String result = Pcap.lookupdev(errbuf);
		System.out.printf("'%s', '%s'\n", result, errbuf);
		assertEquals(dev,result);
	}

	/**
	 * Test of lookupnet method, of class Pcap.
	 */
	@Test
	public void testLookupnet() {
		System.out.println("lookupnet");
		final int expResult = 0;
		final int result = Pcap.lookupnet(dev, net, mask, errbuf);
		System.out.printf("'%s', net: '%x', mask: '%x', err: '%s'\n", result, net, mask, errbuf);
		assertEquals(expResult, result);
		assertTrue(net!=0);
		assertTrue(mask!=0);
		assertEquals(errbuf.toString(),"");
	}

	/**
	 * Test of setSnaplen method, of class Pcap.
	 */
	@Test
	public void testSetSnaplen() {
		System.out.println("setSnaplen");
		final int expResult = 0;
		final int result = Pcap.setSnaplen(ptr, snaplen);
		assertEquals(expResult, result);
	}

	/**
	 * Test of getSnaplen method, of class Pcap.
	 */
	@Test
	public void testGetSnaplen() {
		System.out.println("getSnaplen");
		Pcap.setSnaplen(ptr, snaplen);
		final int expResult = snaplen;
		final int result = Pcap.getSnaplen(ptr);
		assertEquals(expResult, result);
	}

	/**
	 * Test of setPromisc method, of class Pcap.
	 */
	@Test
	public void testSetPromisc() {
		System.out.println("setPromisc");
		final int expResult = 0;
		final int result = Pcap.setPromisc(ptr, promisc);
		assertEquals(expResult, result);
	}

	/**
	 * Test of canSetRfmon method, of class Pcap.
	 */
	@Test
	public void testCanSetRfmon() {
		System.out.println("canSetRfmon");
		final int result = Pcap.canSetRfmon(ptr);
		assertTrue(
				result == 0 ||
				result == 1 ||
				result == Pcap.ERROR
				);
	}

	/**
	 * Test of setRfmon method, of class Pcap.
	 */
	@Test
	public void testSetRfmon() {
		System.out.println("setRfmon");
		final int expResult = 0;
		final int result = Pcap.setRfmon(ptr, rfmon);
		assertEquals(expResult, result);
	}

	/**
	 * Test of setTimeout method, of class Pcap.
	 */
	@Test
	public void testSetTimeout() {
		System.out.println("setTimeout");
		final int expResult = 0;
		final int result = Pcap.setTimeout(ptr, toms);
		assertEquals(expResult, result);
	}

	/**
	 * Test of listTstampTypes method, of class Pcap.
	 */
	@Test
	public void testListTstampTypes() {
		System.out.println("listTstampTypes");
		final ArrayList<Integer> types = new ArrayList<Integer>();
		final int result = Pcap.listTstampTypes(ptr, types);
		assertEquals(types.size(), result);
	}

	/**
	 * Test of setTstampType method, of class Pcap.
	 */
	@Test
	public void testSetTstampType() {
		System.out.println("setTstampType");
		//int expResult, type = -1;
		int expResult = -1; Pcap.TSTAMP type;
		final ArrayList<Integer> types = new ArrayList<Integer>();
		final int nTstampTypes = Pcap.listTstampTypes(ptr, types);
		if (nTstampTypes == 0) {
			//type = Pcap.TSTAMP_HOST;
			type = Pcap.TSTAMP.HOST;
			expResult = Pcap.ERROR_CANTSET_TSTAMP_TYPE;
		} else {
			//type = types.get(0);
			type = Pcap.TSTAMP.fromValue(types.get(0));
			expResult = 0;
		}
		//final int result = Pcap.setTstampType(ptr, type);
		final int result = Pcap.setTstampType(ptr, type.getValue());
		assertEquals(expResult,result);
	}

	/**
	 * Test of tstampTypeNameToVal method, of class Pcap.
	 */
	@Test
	public void testTstampTypeNameToVal() {
		System.out.println("tstampTypeNameToVal");
		final String name = "host";
		//final int expResult = Pcap.TSTAMP_HOST;
		final int expResult = Pcap.TSTAMP.HOST.getValue();
		final int result = Pcap.tstampTypeNameToVal(name);
		assertEquals(expResult, result);
	}

	/**
	 * Test of tstampTypeValToName method, of class Pcap.
	 */
	@Test
	public void testTstampTypeValToName() {
		System.out.println("tstampTypeValToName");
		//final int val = Pcap.TSTAMP_HOST;
		final int val = Pcap.TSTAMP.HOST.getValue();
		final String expResult = "host";
		final String result = Pcap.tstampTypeValToName(val);
		assertEquals(expResult, result);
	}

	/**
	 * Test of tstampTypeValToDescription method, of class Pcap.
	 */
	@Test
	public void testTstampTypeValToDescription() {
		System.out.println("tstampTypeValToDescription");
		//final int val = Pcap.TSTAMP_HOST;
		final int val = Pcap.TSTAMP.HOST.getValue();
		final String expResult = "Host";
		final String result = Pcap.tstampTypeValToDescription(val);
		assertEquals(expResult, result);
	}

	/**
	 * Test of setBuffersize method, of class Pcap.
	 */
	@Test
	public void testSetBuffersize() {
		System.out.println("setBuffersize");
		final int buffersize = 255;
		final int expResult = 0;
		final int result = Pcap.setBuffersize(ptr, buffersize);
		assertEquals(expResult, result);
	}

	/**
	 * Test of activate method, of class Pcap.
	 */
	@Test
	public void testActivate() {
		System.out.println("activate");
		final int result = Pcap.activate(ptr);
		assertTrue(
				result == 0 ||
				result == Pcap.WARNING ||
				result == Pcap.ERROR
				);
	}

	/**
	 * Test of openLive method, of class Pcap.
	 */
	@Test
	public void testOpenLive() {
		System.out.println("openLive");
		final long unExpResult = 0L;
		final long result = Pcap.openLive(dev, snaplen, promisc, toms, errbuf);
		Pcap.close(result);
		assertTrue(unExpResult!=result);
	}

	/**
	 * Test of openDead method, of class Pcap.
	 */
	@Test
	public void testOpenDead() {
		System.out.println("openDead");
		final int linktype = 0;
		final long unExpResult = 0L;
		final long result = Pcap.openDead(linktype, snaplen);
		assertTrue(unExpResult!=result);
		//fail("need to get linktypes!");
	}

	/**
	 * Test of openOffline method, of class Pcap.
	 */
	@Test
	public void testOpenOffline() {
		System.out.println("openOffline");
		final String fname = "test/test.pcap";
		final long unExpResult = 0L;
		final long result = Pcap.openOffline(fname, errbuf);
		Pcap.close(result);
		assertTrue(unExpResult!=result);
	}

	/**
	 * Test of loop method, of class Pcap.
	 */
	@Test
	public void testLoop() {
		System.out.println("loop");
		final int cnt = 10;
		final PcapHandler <String> callback = new PacketCounter();
		final String user = "";
		final int expResult = 0;
		Pcap.close(ptr);
		ptr = Pcap.openLive(dev, snaplen, promisc, toms, errbuf);
		final int result = Pcap.loop(ptr, cnt, callback, user);
		assertEquals(expResult, result);
		assertEquals(cnt,callback.count());
	}

	/**
	 * Test of dispatch method, of class Pcap.
	 */
	@Test
	public void testDispatch() {
		System.out.println("dispatch");
		final int cnt = 10;
		final PcapHandler<String> callback = new PacketCounter();
		final String user = "";
		Pcap.close(ptr);
		ptr = Pcap.openLive(dev, snaplen, promisc, toms, errbuf);
		final int result = Pcap.dispatch(ptr, cnt, callback, user);
		assertTrue(result>=0);
	}
}
