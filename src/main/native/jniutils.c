/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <jni.h>
#include "jniutils.h"

// CLASS REFERENCES
jclass cls_StringBuilder = NULL;
jclass cls_Integer = NULL;
jclass cls_ArrayList = NULL;

// FIELD IDS
jfieldID fid_Integer_value = 0;

// METHOD IDS
jmethodID mid_Integer_init_i = 0;
jmethodID mid_StringBuilder_append = 0;
jmethodID mid_StringBuilder_setLength = 0;
jmethodID mid_ArrayList_add_object = 0;

/*
 * Throws an exception and return immediately.
 * @param 	env				JNI environment pointer
 * @param 	exname 		fully qualified exception class identifier.
 * @param 	message 	the message to use
 */
void
throwException(JNIEnv *env, const char *exname, const char *message)
{
	jclass exception = (*env)->FindClass (env,exname);
	jthrowable ex = (*env)->ExceptionOccurred (env); // might not need this, check
	
	if (exception != NULL) {
		(*env)->ThrowNew (env, exception, message);
	}
}

/*
 * Find a class. If not found throw Exception. Else create a global
 * reference and return it
 * @param 	env				JNI environment pointer
 * @param 	clsname 	fully qualified class identifier.
 * @return 	A global reference to the class. Needs to be freed manually
 */
jclass
findClass (JNIEnv *env, const char *clsname)
{
	jclass local;
	if ( (local = (*env)->FindClass (env,clsname)) == NULL) {
		throwException (env, "Ljava/lang/ClassNotFoundException;", clsname);
		return NULL;
	}

	jclass global = (jclass) (*env)->NewGlobalRef (env, local);
	(*env)->DeleteLocalRef (env, local);
	if (global == NULL) {
		return NULL; // Out of memory exception already thrown
	}
	return global;
}

/**
 * Loads the Integer class and sets all necessary IDs
 * @param 	env 	JNI environment pointer
 */
void
loadInteger (JNIEnv *env)
{
	cls_Integer = findClass (env, "Ljava/lang/Integer;");
	if (cls_Integer == NULL) return;
	
	fid_Integer_value = (*env)->GetFieldID (env, cls_Integer, "value", "I");
	mid_Integer_init_i = (*env)->GetMethodID (env,cls_Integer,"<init>","(I)V");
	if (mid_Integer_init_i == NULL) return;
}

/**
 * Creates a new Integer instance
 */
jobject NewInteger(JNIEnv *env, int i)
{
	return (*env)->NewObject(env, cls_Integer, mid_Integer_init_i, i);
}

/**
 * Sets the value of a java Integer
 * @param 	env			JNI environment pointer
 * @param 	obj 		the java Integer object
 * @param 	value		value to set
 */
inline void
setIntegerValue (JNIEnv *env, jobject obj, int value)
{
	if (obj == NULL) throwException (env, "Ljava/lang/NullPointerException;", NULL);
	(*env)->SetIntField (env, obj, fid_Integer_value, value);
}

/**
 * Loads the StringBuilder class and sets all necessary IDs
 * @param		env 	JNI environment pointer
 */
void
loadStringBuilder (JNIEnv *env)
{
	cls_StringBuilder = findClass (env, "Ljava/lang/StringBuilder;");
	if (cls_StringBuilder == NULL) return;
	
	mid_StringBuilder_append = (*env)->GetMethodID (env,
		cls_StringBuilder, "append",
		"(Ljava/lang/String;)Ljava/lang/StringBuilder;"
	);
	if (mid_StringBuilder_append == NULL) return;
	
	mid_StringBuilder_setLength = (*env)->GetMethodID (env,
		cls_StringBuilder, "setLength",
		"(I)V"
	);
	if (mid_StringBuilder_setLength) return;
}

/**
 * Builds a String by calling StringBuilder functions.
 * @param 	env				JNI environment pointer
 * @param 	builder 	the StringBuilder object used to build the string
 * @param 	off 			offset into the builder
 * @param 	str 			the string to append
 */
inline void
buildString (JNIEnv *env, jobject builder, unsigned int off, const char *str)
{
	if (str == NULL) str = "";
	jstring jstr = (*env)->NewStringUTF (env, str);
	(*env)->CallVoidMethod (env, builder, mid_StringBuilder_setLength, off);
	(*env)->CallObjectMethod (env, builder, mid_StringBuilder_append, jstr);
	return;
}

/**
* Loads the ArrayList class and sets all necessary IDs
* @param		env 	JNI environment pointer
 */
void
loadArrayList (JNIEnv *env)
{
	cls_ArrayList = findClass (env, "Ljava/util/ArrayList;");
	if (cls_ArrayList == NULL) return;
	
	mid_ArrayList_add_object = (*env)->GetMethodID (env,
		cls_ArrayList, "add",
		"(Ljava/lang/Object;)Z"
	);
	if (mid_ArrayList_add_object == NULL) return;
}
