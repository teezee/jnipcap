/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <jni.h>
#include <pcap.h>
#include "jnipcapids.h"
#include "jnipcap.h"
#include "jniutils.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    initIDs
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_disy_jnipcap_PcapPacketHeader_initIDs (JNIEnv *env, jclass jcls)
{
	printf("PcapPacketHeader_initIDs: cls\n");
	cls_PcapPacketHeader = (jclass) (*env)->NewGlobalRef (env, jcls);
	
	printf("PcapPacketHeader_initIDs: mid\n");
	mid_PcapPacketHeader_init = (*env)->GetMethodID (env, cls_PcapPacketHeader, "<init>", "()V");
	if (mid_PcapPacketHeader_init == NULL) return;
	
	printf("PcapPacketHeader_initIDs: fid\n");
	fid_PcapPacketHeader_address = (*env)->GetFieldID (env, cls_PcapPacketHeader, "address", "J");
	if (fid_PcapPacketHeader_address == NULL) return;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    sec
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_PcapPacketHeader_sec__ (JNIEnv *env, jobject jobj)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	return (jlong) p->ts.tv_sec;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    sec
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_PcapPacketHeader_sec__J (JNIEnv *env, jobject jobj, jlong jsec)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	p->ts.tv_sec = (long) jsec;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    usec
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_PcapPacketHeader_usec__ (JNIEnv *env, jobject jobj)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	return (jlong) p->ts.tv_usec;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    usec
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_PcapPacketHeader_usec__J (JNIEnv *env, jobject jobj, jlong jusec)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	p->ts.tv_usec = (long) jusec;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    caplen
 * Signature: ()I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_PcapPacketHeader_caplen (JNIEnv *env, jobject jobj)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	return (jint) p->caplen;
}

/*
 * Class:     disy_jnipcap_PcapPacketHeader
 * Method:    len
 * Signature: ()I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_PcapPacketHeader_len (JNIEnv *env, jobject jobj)
{
	struct pcap_pkthdr *p = (struct pcap_pkthdr *) (*env)->GetLongField (env, jobj, fid_PcapPacketHeader_address);
	if (p == NULL) return -1;
	return (jint) p->len;
}

#ifdef __cplusplus
}
#endif
