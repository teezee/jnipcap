/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <jni.h>
#include "jnipcaputils.h"
#include "jnipcapids.h"

void bytebuffer_handler (u_char *user, const struct pcap_pkthdr *h, const u_char *bytes)
{	
	pcap_callback_t *data = (pcap_callback_t*) user;
	JNIEnv *env = data->env;
	
	(*env)->SetLongField (env, data->pkthdr, fid_PcapPacketHeader_address, (jlong) h);
	
	jobject jbuffer = (*env)->NewDirectByteBuffer(env, (void*) bytes, h->caplen);
	if (jbuffer == NULL) return;
		
	(*env)->CallVoidMethod(env,
		data->obj,
		data->mid,
		(jobject) data->user,
		(jobject) data->pkthdr,
		jbuffer
	);
	
	(*env)->DeleteLocalRef(env,jbuffer);
	
	if ((*env)->ExceptionCheck(env) == JNI_TRUE) {
		pcap_breakloop(data->p);
	}
}
