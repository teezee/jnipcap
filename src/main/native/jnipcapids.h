/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _Included_disy_jnipcap_jnipcapids
#define _Included_disy_jnipcap_jnipcapids
#ifdef __cplusplus
extern "C" {
#endif

// somehow this always leads to
// duplicate symbols, ... y?
// TODO: check and clean up!

/*
#ifndef cls_Pcap
jclass cls_Pcap = NULL;
#endif

#ifndef mid_Pcap_init
jmethodID mid_Pcap_init = NULL;
#endif

#ifndef cls_PcapHandler
jclass cls_PcapHandler = NULL;
#endif

#ifndef mid_PcapHandler_receive
jmethodID mid_PcapHandler_receive = NULL;
#endif

#ifndef cls_PcapPacketHeader
jclass cls_PcapPacketHeader = NULL;
#endif

#ifndef mid_PcapPacketHeader_init
jmethodID mid_PcapPacketHeader_init = NULL;
#endif

#ifndef fid_PcapPacketHeader_address
jfieldID fid_PcapPacketHeader_address = NULL;
#endif
*/

jclass cls_Pcap;
jmethodID mid_Pcap_init;

jclass cls_PcapHandler;
jmethodID mid_PcapHandler_receive;

jclass cls_PcapPacketHeader;
jmethodID mid_PcapPacketHeader_init;
jfieldID fid_PcapPacketHeader_address;

#ifdef __cplusplus
}
#endif
#endif
