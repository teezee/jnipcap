/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _Included_jniutils
#define _Included_jniutils
#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>

#ifndef WIN32

#include <stdlib.h>
#define pointer_t intptr_t;

#endif // ndef WIN32


// CLASS REFERENCES
extern jclass cls_StringBuilder;
extern jclass cls_Integer;
extern jclass cls_ArrayList;

// FIELD IDS
extern jfieldID fid_Integer_value;

// METHOD IDS
extern jmethodID mid_Integer_init_i;
extern jmethodID mid_StringBuilder_append;
extern jmethodID mid_StringBuilder_setLength;
extern jmethodID mid_ArrayList_add_object;
	
// Exceptions
//#define CLASSNOTFOUNDEXCEPTION "Ljava/lang/ClassNotFoundException;"

// Classes
//#define STRINGBUILDER "Ljava/lang/StringBuilder;"

void loadStringBuilder (JNIEnv *env);
void loadInteger (JNIEnv *env);
void loadArrayList (JNIEnv *env);
jobject NewInteger(JNIEnv *env, int i);
void throwException (JNIEnv *env, const char *exname, const char *message);
jclass findClass (JNIEnv *env, const char *clsname);
inline void buildString (JNIEnv *env, jobject builder, unsigned int off, const char *str);
inline void setIntegerValue (JNIEnv *env, jobject obj, int value);

#ifdef __cplusplus
}
#endif
#endif
