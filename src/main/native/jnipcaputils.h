/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#ifndef _Included_jnipcaputils
#define _Included_jnipcaputils
#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>
#include <pcap.h>

typedef struct pcap_callback {
	JNIEnv *env;			// environment
	jclass cls; 		// callback class
	jobject obj; 			// callback object
	jmethodID mid; 		// callback method id
	jobject user; 		// java user data object
	jobject pkthdr; 	// java pcap header object
	jthrowable  exception; // Any exceptions to rethrow
	pcap_t *p; 				// pcap pointer
} pcap_callback_t;

void bytebuffer_handler (u_char *user, const struct pcap_pkthdr *h, const u_char *bytes);

#ifdef __cplusplus
}
#endif
#endif
