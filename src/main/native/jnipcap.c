/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
#include <jni.h>
#include <pcap.h>
#include <string.h> 			// memset
#include "jnipcap.h"
#include "jnipcapids.h"
#include "jniutils.h"
#include "jnipcaputils.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    initIDs
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_disy_jnipcap_Pcap_initIDs (JNIEnv *env, jclass cls)
{
	cls_Pcap = (jclass) (*env)->NewGlobalRef (env, cls);
	
	mid_Pcap_init = (*env)->GetMethodID (env, cls, "<init>", "()V");
	if (mid_Pcap_init == NULL) return;
	
	cls_PcapHandler = findClass(env, "Ldisy/jnipcap/PcapHandler;");
	mid_PcapHandler_receive = (*env)->GetMethodID(env, 
		cls_PcapHandler, "receive", 
		"(Ljava/lang/Object;Ldisy/jnipcap/PcapPacketHeader;Ljava/nio/ByteBuffer;)V"
	);
	if (mid_PcapHandler_receive == NULL) return;
	
	Java_disy_jnipcap_PcapPacketHeader_initIDs(env, findClass(env, "disy/jnipcap/PcapPacketHeader"));

	loadStringBuilder(env);
	loadInteger(env);
	loadArrayList(env);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    lookupdev
 * Signature: (Ljava/lang/StringBuilder;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_disy_jnipcap_Pcap_lookupdev (JNIEnv *env, jclass cls, jobject jerrbuf)
{
	if (jerrbuf == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", "errbuf must not be NULL");
		return NULL;
	}
	
	char errbuf[PCAP_ERRBUF_SIZE];
	errbuf[0] = '\0';
	char *dev = pcap_lookupdev(errbuf);
	
	buildString (env, jerrbuf, 0, errbuf);
	if (dev == NULL) {
		throwException (env, "Ljava/lang/Exception;", errbuf);
		return NULL;
	}
	
	return (*env)->NewStringUTF (env, dev);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    lookupnet
 * Signature: (Ljava/lang/String;IILjava/lang/StringBuilder;)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_lookupnet (JNIEnv *env, jclass cls, jstring jdev, jobject jnet, jobject jmask, jobject jerrbuf)
{
	if (jdev == NULL || jnet == NULL || jmask == NULL || jerrbuf == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return -1;
	}
	
	char errbuf[PCAP_ERRBUF_SIZE];
	errbuf[0] = '\0';
	const char *dev = (*env)->GetStringUTFChars (env, jdev, 0);
	bpf_u_int32 net, mask;
	int ret = pcap_lookupnet (dev, &net, &mask, errbuf);

	setIntegerValue (env, jnet, net);
	setIntegerValue (env, jmask, mask);
	buildString (env, jerrbuf, 0, errbuf);

	(*env)->ReleaseStringUTFChars (env, jdev, dev);
	return (jint) ret;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setSnaplen
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setSnaplen (JNIEnv *env, jclass jcls, jlong jptr, jint jsnaplen)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_snaplen (p, (int) jsnaplen);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    snapshot
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_snapshot (JNIEnv *env, jclass jcls, jlong jptr)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_snapshot (p);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setPromisc
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setPromisc (JNIEnv *env, jclass jcls, jlong jptr, jint jpromisc)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_promisc (p, (int) jpromisc);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    canSetRfmon
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_canSetRfmon (JNIEnv *env, jclass jcls, jlong jptr)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_can_set_rfmon (p);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setRfmon
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setRfmon (JNIEnv *env, jclass jcls, jlong jptr, jint jmon)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_rfmon (p, (int) jmon);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setTimeout
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setTimeout (JNIEnv *env, jclass jcls, jlong jptr, jint jtoms)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_timeout (p, (int) jtoms);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setTstampType
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setTstampType (JNIEnv *env, jclass jcls, jlong jptr, jint jtype)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_tstamp_type (p, (int) jtype);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    listTstampTypes
 * Signature: (JLjava/util/ArrayList;)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_listTstampTypes (JNIEnv *env, jclass jcls, jlong jptr, jobject jtypes)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	int *carr;
	int ret = pcap_list_tstamp_types (p, &carr);
	if (ret >= 0) {
		int i = 0;
		for (;i<ret;i++) {
			jobject ji = NewInteger(env,carr[i]);
			(*env)->CallBooleanMethod (env, jtypes, mid_ArrayList_add_object, ji);
		}
	}
	pcap_free_tstamp_types(carr);
	return ret;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    tstampTypeNameToVal
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_tstampTypeNameToVal (JNIEnv *env, jclass jcls, jstring jname)
{
	const char *name = (*env)->GetStringUTFChars(env, jname, 0);
	jint val = (jint) pcap_tstamp_type_name_to_val(name);
	(*env)->ReleaseStringUTFChars(env, jname, name);
	return val;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    tstampTypeValToName
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_disy_jnipcap_Pcap_tstampTypeValToName (JNIEnv *env, jclass jcls, jint jval)
{
	const char *name = pcap_tstamp_type_val_to_name((int)jval);
	return (*env)->NewStringUTF (env, name);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    tstampTypeValToDescription
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_disy_jnipcap_Pcap_tstampTypeValToDescription (JNIEnv *env, jclass jcls, jint jval)
{
	const char *desc = pcap_tstamp_type_val_to_description((int)jval);
	return (*env)->NewStringUTF (env, desc);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    setBuffersize
 * Signature: (JI)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_setBuffersize (JNIEnv *env, jclass jcls, jlong jptr, jint jsize)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_set_buffer_size (p, (int) jsize);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    activate
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_activate (JNIEnv *env, jclass jcls, jlong jptr)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p == NULL) return -1;
	return (jint) pcap_activate (p);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    create
 * Signature: (Ljava/lang/String;Ljava/lang/StringBuilder;)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_Pcap_create (JNIEnv *env, jclass jcls, jstring jdev, jobject jerrbuf)
{
	if (jdev == NULL || jerrbuf == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return (jlong) NULL;
	}

	char errbuf[PCAP_ERRBUF_SIZE];
	errbuf[0] = '\0';
	const char *dev = (*env)->GetStringUTFChars (env, jdev, 0);
	pcap_t *p = pcap_create (dev, errbuf);

	(*env)->ReleaseStringUTFChars (env, jdev, dev);
	buildString (env, jerrbuf, 0, errbuf);
	
	if (p == NULL) return (jlong) NULL;
	if (mid_Pcap_init == NULL) {
		pcap_close (p);
		return (jlong) NULL;
	}
	
	return (jlong) p;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    openLive
 * Signature: (Ljava/lang/String;IIILjava/lang/StringBuilder;)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_Pcap_openLive (JNIEnv *env, jclass jcls, jstring jdev, jint jsnaplen, jint jpromisc, jint jtoms, jobject jerrbuf)
{
	if (jdev == NULL || jerrbuf == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return (jlong) NULL;
	}
	char errbuf[PCAP_ERRBUF_SIZE];
	errbuf[0] = '\0';
	const char *dev = (*env)->GetStringUTFChars (env, jdev, 0);
	
	pcap_t *p = pcap_open_live(dev, (int)jsnaplen, (int)jpromisc, (int)jtoms, errbuf);
	
	(*env)->ReleaseStringUTFChars (env, jdev, dev);
	buildString (env, jerrbuf, 0, errbuf);
	
	if (p == NULL) return (jlong) NULL;
	if (mid_Pcap_init == NULL) {
		pcap_close (p);
		return (jlong) NULL;
	}
	
	return (jlong) p;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    openDead
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_Pcap_openDead (JNIEnv *env, jclass jcls, jint jlinktype, jint jsnaplen)
{
	pcap_t *p = pcap_open_dead((int)jlinktype, (int)jsnaplen);
	if (p == NULL) return (jlong) NULL;
	if (mid_Pcap_init == NULL) {
		pcap_close (p);
		return (jlong) NULL;
	}
	return (jlong) p;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    openOffline
 * Signature: (Ljava/lang/String;Ljava/lang/StringBuilder;)J
 */
JNIEXPORT jlong JNICALL
Java_disy_jnipcap_Pcap_openOffline (JNIEnv *env, jclass jcls, jstring jfname, jobject jerrbuf)
{
	if (jfname == NULL || jerrbuf == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return (jlong) NULL;
	}
	char errbuf[PCAP_ERRBUF_SIZE];
	errbuf[0] = '\0';
	const char *fname = (*env)->GetStringUTFChars (env, jfname, 0);
	
	pcap_t *p = pcap_open_offline(fname, errbuf);
	(*env)->ReleaseStringUTFChars (env, jfname, fname);
	buildString (env, jerrbuf, 0, errbuf);
	
	if (p == NULL) return (jlong) NULL;
	if (mid_Pcap_init == NULL) {
		pcap_close (p);
		return (jlong) NULL;
	}
	
	return (jlong) p;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    close
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_disy_jnipcap_Pcap_close (JNIEnv *env, jclass jcls, jlong jptr)
{
	pcap_t *p = (pcap_t *) jptr;
	if (p != NULL) pcap_close(p);
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    loop
 * Signature: (JILdisy/jnipcap/PcapHandler;Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_loop (JNIEnv *env, jclass jcls, jlong jptr, jint jcnt, jobject jcallback, jobject juser)
{
	pcap_t *p = (pcap_t *) jptr;
	// check for null pointers
	if (p == NULL || jcallback == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return -1;
	}
	// init handler data
	pcap_callback_t user;
	memset(&user, 0, sizeof(user));
	user.env = env;
	user.cls = (*env)->GetObjectClass(env, jcallback);
	user.obj = jcallback;
	user.mid = mid_PcapHandler_receive;
	user.user = juser;
	user.pkthdr = (*env)->NewObject(env, cls_PcapPacketHeader, mid_PcapPacketHeader_init);
	user.p = p;
	// do it
	int r = pcap_loop(p, (int)jcnt, bytebuffer_handler, (u_char*) &user);
	return r;
}

/*
 * Class:     disy_jnipcap_Pcap
 * Method:    dispatch
 * Signature: (JILdisy/jnipcap/PcapHandler;Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL
Java_disy_jnipcap_Pcap_dispatch (JNIEnv *env, jclass jcls, jlong jptr, jint jcnt, jobject jcallback, jobject juser)
{
	pcap_t *p = (pcap_t *) jptr;
	// check for null pointers
	if (p == NULL || jcallback == NULL) {
		throwException (env, "Ljava/lang/NullPointerException;", NULL);
		return -1;
	}
	// init handler data
	pcap_callback_t user;
	memset(&user, 0, sizeof(user));
	user.env = env;
	user.cls = (*env)->GetObjectClass(env, jcallback);
	user.obj = jcallback;
	user.mid = mid_PcapHandler_receive;
	user.user = juser;
	user.pkthdr = (*env)->NewObject(env, cls_PcapPacketHeader, mid_PcapPacketHeader_init);
	user.p = p;
	// do it
	int r = pcap_dispatch(p, (int)jcnt, bytebuffer_handler, (u_char*) &user);
	return r;
}


#ifdef __cplusplus
}
#endif
