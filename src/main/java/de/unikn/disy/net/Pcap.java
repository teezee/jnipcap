/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
package de.unikn.disy.net;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zink
 *
 */
public final class Pcap {
	static {
		System.loadLibrary("jnipcap");
		initIDs();
	}

	/*
	 * Time stamp types.
	 * Not all systems and interfaces will necessarily support all of these.
	 * See {@link tstamp} for a list of all the time stamp types.
	 */
	public static enum TSTAMP {
		HOST(0), HOST_LOWPREC(1), HOST_HIPREC(2), ADAPTER(3), ADAPTER_UNSYNCED(4);

		private static final Map<Integer,TSTAMP> valToTstamp = new HashMap<Integer,TSTAMP>();
		private static final Map<String,TSTAMP> nameToTstamp = new HashMap<String,TSTAMP>();
		private static final Map<String,TSTAMP> descToTstamp = new HashMap<String,TSTAMP>();

		static {
			for (final TSTAMP s: values()) {
				valToTstamp.put(s.getValue(), s);
				nameToTstamp.put(s.getName(), s);
				descToTstamp.put(s.getDescription(), s);
			}
		}

		private final int val;
		private final String name;
		private final String desc;

		TSTAMP(final int val) {
			this.val = val;
			name = Pcap.tstampTypeValToName(val);
			desc = Pcap.tstampTypeValToDescription(val);
		}

		public static TSTAMP fromValue(final int value) { return valToTstamp.get(value); }
		public static TSTAMP fromName(final String name) { return nameToTstamp.get(name); }
		public static TSTAMP fromDescription(final String desc) { return descToTstamp.get(desc); }

		public final int getValue() { return val; }
		public final String getName() { return name; }
		public final String getDescription() { return desc; }
		@Override public final String toString() { return name; }
	}
	//public final static int TSTAMP_HOST = 0;	/* host-provided, unknown characteristics */
	//public final static int TSTAMP_HOST_LOWPREC =1;	/* host-provided, low precision */
	//public final static int TSTAMP_HOST_HIPREC = 2;	/* host-provided, high precision */
	//public final static int TSTAMP_ADAPTER = 3;	/* device-provided, synced with the system clock */
	//public final static int TSTAMP_ADAPTER_UNSYNCED = 4;	/* device-provided, not synced with the system clock */

	/*
	 * static definitions
	 * TODO check if we better put these into enums, generated through native calls
	 */

	/*
	 * Error codes for the pcap API.
	 * These will all be negative, so you can check for the success or
	 * failure of a call that returns these codes by checking for a
	 * negative value.
	 */
	public final static int ERROR = -1; /* generic error code */
	public final static int ERROR_BREAK = -2; /* loop terminated by pcap_breakloop */
	public final static int ERROR_NOT_ACTIVATED = -3; /* the capture needs to be activated */
	public final static int ERROR_ACTIVATED = -4; /* the operation can't be performed on already activated captures */
	public final static int ERROR_NO_SUCH_DEVICE = -5; /* no such device exists */
	public final static int ERROR_RFMON_NOTSUP = -6; /* this device doesn't support rfmon (monitor) mode */
	public final static int ERROR_NOT_RFMON = -7; /* operation supported only in monitor mode */
	public final static int ERROR_PERM_DENIED = -8; /* no permission to open the device */
	public final static int ERROR_IFACE_NOT_UP = -9; /* interface isn't up */
	public final static int ERROR_CANTSET_TSTAMP_TYPE = -10; /* this device doesn't support setting the time stamp type */
	public final static int ERROR_PROMISC_PERM_DENIED = -11; /* you don't have permission to capture in promiscuous mode */

	/*
	 * Warning codes for the pcap API.
	 * These will all be positive and non-zero, so they won't look like
	 * errors.
	 */
	public final static int WARNING = 1; /* generic warning code */
	public final static int WARNING_PROMISC_NOTSUP= 2; /* this device doesn't support promiscuous mode */
	public final static int WARNING_TSTAMP_TYPE_NOTSUP = 3; /* the requested time stamp type is not supported */

	public final static long NETMASK_UNKNOWN = 0xffffffffL;

	/**
	 * @name initIDs
	 * @brief initialize IDs
	 * Initializes static class, field and method ids referenced
	 * by the Pcap class.
	 */
	private static native void initIDs();

	/**
	 * @brief Find the default device on which to capture.
	 *
	 * Returns a reference to a String giving the name of a
	 * network device suitable for use with create() and activate(),
	 * or with open_live() and with lookupnet(). If there is an
	 * error, Null is returned and errbuf is filled in with an appropriate
	 * error message.
	 *
	 * C function: {@code char *pcap_lookupdev(char *errbuf);}
	 *
	 * @param 		errbuf
	 * 						StringBuilder object that takes the error String
	 * @return
	 * 						a String reference on success or
	 * 						Null reference otherwise
	 */
	public static native String lookupdev(StringBuilder errbuf);

	/**
	 * @brief Find the IPv4 network number and netmask for a device
	 *
	 * Is used to determine the IPv4 network number and mask
	 * associated with the network device device.
	 * Note: both net and mask must be Integer references. Native ints
	 * won't work.
	 *
	 * C function: {@code int pcap_lookupnet(const char *device, bpf_u_int32 *netp, bpf_u_int32 *maskp, char *errbuf);}
	 *
	 * @param 		dev
	 * 						String reference of the device name
	 * @param 		net
	 * 						Integer reference to hold the network number
	 * @param 		mask
	 * 						Integer reference to hold the network mask
	 * @param 		errbuf
	 * 						StringBuilder object that takes the error String
	 * @return
	 * 						0 on success or -1 on failure
	 */
	public static native int lookupnet(String dev, Integer net, Integer mask, StringBuilder errbuf);

	/*
		Members
	 */
	/**
	 * Sets the snapshot length to be used on a capture handle when the handle
	 * is activated to snaplen.
	 * Since libpcap version 1.0.0
	 *
	 * C function: {@code int pcap_set_snaplen(pcap_t *p, int snaplen);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	snaplen
	 * 					snapshot length to set
	 * @return
	 * 					0 on success or
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 **/
	public static native int setSnaplen (long ptr, int snaplen);

	/**
	 * @brief Get snapshot length
	 *
	 * Convenience method for {@link snapshot()}.
	 */
	public static int getSnaplen (final long ptr) { return snapshot(ptr); }

	/**
	 * @brief Get the snapshot length
	 *
	 * Returns the snapshot length specified when {@link setSnaplen()}
	 * or {@link openLive()} was called, or the snapshot length from
	 * the capture file, for a ``savefile''.
	 *
	 * C function: {@code int pcap_snapshot(pcap_t *p);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @return
	 * 					the snapshot length
	 */
	public static native int snapshot (final long ptr);

	/**
	 * @brief Set promiscuous mode for a not-yet-activated capture handle.
	 *
	 * Sets whether promiscuous mode should be set on a capture handle
	 * when the handle is activated. If promisc is non-zero,
	 * promiscuous mode will be set, otherwise it will not be set.
	 * Since libpcap version 1.0.0
	 *
	 * C function: {int	pcap_set_promisc(pcap_t *p, int promisc);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	promisc
	 * 					If 0, promisc will not be set, else will be set
	 * @return
	 *					0 on success
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 **/
	public static native int setPromisc (final long ptr, final int promisc);

	/**
	 * @brief Check whether monitor mode can be set for a not-yet-activated
	 * 				capture handle.
	 *
	 * Check whether monitor mode can be set for a
	 * not-yet-activated capture handle.
	 * 
	 * C function: {@code int	pcap_can_set_rfmon(pcap_t *p);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @return
	 * 				0 if monitor mode could not be set
	 * 				1 if monitor mode could be set
	 * 				{@link NO_SUCH_DEVICE} if the  device  specified when the handle was created doesn't exist
	 * 				{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 * 				{@link ERROR} if an error ocurred, call {@link getError} or {@link pError} to
	 * 											fetch or print the error text.
	 */
	public static native int canSetRfmon (final long ptr);

	/**
	 * @brief Set monitor mode for a not-yet-activated capture handle.
	 * 
	 * Sets whether monitor mode should be set on a capture handle when the
	 * handle is activated.
	 *
	 * C function: {@code int pcap_set_rfmon(pcap_t *p, int rfmon);}
	 * 
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	rfmon
	 * 					if non-zero, monitor mode will be set, else will not be set
	 * @return
	 * 					0 on success
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 */
	public static native int setRfmon (long ptr, int rfmon);

	/**
	 * @brief Set the read timeout for a not-yet-activated capture handle
	 * 
	 * Sets the read timeout that will be used on a capture handle when the
	 * handle is activated
	 *
	 * C function: {@code int pcap_set_timeout(pcap_t *p, int to_ms);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	toms
	 * 					desired timeout in milliseconds
	 * @return
	 * 					0 on success
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 */
	public static native int setTimeout (long ptr, int toms);

	/**
	 * @brief Set the time stamp type to be used by a capture device
	 *
	 * Sets the the type of time stamp desired for packets captured on the
	 * pcap descriptor to the type specified. It must be called on a pcap
	 * descriptor  created by {@link create()} that has not yet been activated
	 * by {@link activate()}. {@link listTstampTypes()} will give a list of
	 * the time stamp types supported by a given capture device.
	 *
	 * C function: {@code int pcap_set_tstamp_type(pcap_t *p, int tstamp_type);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	type 	desired type
	 * @return
	 * 					0 on success
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 * 					{@link WARNING_TSTAMP_TYPE_NOTSUP} if the specified type is not supported by the capture device
	 * 					{@link ERROR_CANTSET_TSTAMP_TYPE} if the capture device doesn't support setting the time stamp type
	 */
	public static native int setTstampType (long ptr, int type);

	/**
	 * @brief Get a list of time stamp types supported by a capture device,
	 * and free that list.
	 * 
	 * Is used to get a list of the supported time stamp types of the interface
	 * associated with the pcap descriptor. It allocates a native array to hold
	 * the list and adds the content of that list to types. {@see tstamp} for
	 * a list of all the time stamp types. The native method frees the array
	 * with pcap_free_tstamp_types() after passing the values to the jvm. So
	 * there is no wrapper function for pcap_free_tstamp_types().
	 *
	 * C function: {@code int pcap_list_tstamp_types(pcap_t *p, int **tstamp_typesp);}
	 * C function: {@code void pcap_free_tstamp_types(int *tstamp_types);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	types 	an empty ArrayList to hold the types
	 * @return
	 * 					number of time stamp types on success
	 * 					0 for no supported tstamp type
	 * 					{@link ERROR} on failure
	 */
	public static native int listTstampTypes (long ptr, ArrayList<Integer> types);

	//int	pcap_tstamp_type_name_to_val(const char *);
	public static native int tstampTypeNameToVal (String name);

	//const char *pcap_tstamp_type_val_to_name(int);
	public static native String tstampTypeValToName(int val);

	//const char *pcap_tstamp_type_val_to_description(int);
	public static native String tstampTypeValToDescription(int val);

	/**
	 * @brief Set the buffer size for a not-yet-activated capture handle
	 *
	 * Sets the buffer size that will be used on a capture handle when the
	 * handle is activated to buffer_size, which is in units of bytes.
	 *
	 * C function: {@code int pcap_set_buffer_size(pcap_t *p, int buffer_size);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @param 	buffersize 	the buffer size in bytes
	 * @return
	 * 					0 on success
	 * 					{@link ERROR_ACTIVATED} if called on a capture handle that has been activated
	 */
	public static native int setBuffersize(long ptr, int size);

	/**
	 * @brief activate a capture handle
	 *
	 * Used to activate a packet capture handle to look at packets on the
	 * network, with the options that were set on the handle being in effect.
	 *
	 * C function: {@code int pcap_activate(pcap_t *p);}
	 *
	 * @param 	ptr
	 * 					pointer (address) to native pcap_t
	 * @return
	 * 				0 on success
	 * 				WARNING_PROMISC_NOTSUP on success on  a device  that  doesn't  support  promiscuous mode if promiscuous mode was requested
	 * 				...
	 */
	public static native int activate(long ptr);

	/**
	 * @brief create a live capture handle
	 *
	 * Used to create a packet capture handle to look at packets on the network.
	 * source is a string that specifies the network device to open; on Linux
	 * systems with 2.2 or later kernels, a source argument of "any" or NULL
	 * can be used to capture packets from all interfaces.
	 * The returned handle must be activated with pcap_activate() before packets
	 * can be captured with it; options for the capture, such as promiscuous
	 * mode, can be set on the handle before activating it.
	 *
	 * C function: {@code pcap_t *pcap_create(const char *source, char *errbuf);}
	 * since 1.0.0
	 *
	 * @param 		source
	 * 						string, network device to open the handle on
	 * @param 		errbuf
	 * 						StringBuilder object that takes the error String
	 * @return
	 * 						a pcap_t * on success
	 * 						null on failure
	 */
	public static native long create(String device, StringBuilder errbuf);

	// pcap_t *pcap_open_live(const char *device, int snaplen, int promisc, int to_ms, char *errbuf);
	public static native long openLive(String device, int snaplen, int promisc, int toMs, StringBuilder errbuf);

	// pcap_t *pcap_open_dead(int linktype, int snaplen);
	public static native long openDead(int linktype, int snaplen);

	// pcap_t *pcap_open_offline(const char *fname, char *errbuf);
	public static native long openOffline(String fname, StringBuilder errbuf);

	// void	pcap_close(pcap_t *);
	public static native void close(long ptr);

	// int pcap_loop(pcap_t *p, int cnt, pcap_handler callback, u_char *user);
	public static native <T> int loop(long ptr, int cnt, PcapHandler<T> callback, Object user);

	// int pcap_dispatch(pcap_t *p, int cnt, pcap_handler callback, u_char *user);
	public static native <T> int dispatch(long ptr, int cnt, PcapHandler<T> callback, Object user);
}
