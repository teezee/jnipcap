/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
package de.unikn.disy.net;

import java.nio.ByteBuffer;

/**
 * Callback interface as wrapper for pcap_handler
 * 
 * @author zink
 */
public interface PcapHandler<T> {
	/*
	 * native callback:
	 * typedef void (*pcap_handler)(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes);
	 */
	public void receive (T user, PcapPacketHeader header, ByteBuffer bytes);

	/*
	 * @return number of processed packets
	 */
	public int count();
}
