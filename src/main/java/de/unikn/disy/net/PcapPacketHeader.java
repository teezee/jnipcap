/*
 Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

 Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
 DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
*/
package de.unikn.disy.net;

/**
 * Peer class for the native structs 
 * pcap_pkthdr and timeval
 * 
 * @author zink
 */
public final class PcapPacketHeader {
	/* native structs */
	/*
	struct pcap_pkthdr {
		struct timeval ts;
		bpf_u_int32 caplen;
		bpf_u_int32 len;
	};
	struct timeval {
		long	tv_sec;
		long	tv_usec;
	};
	*/
	
	/**
	 * Initialize class ids
	 */
	private static native void initIDs();

	static { 
		System.loadLibrary("jnipcap");
		initIDs();
	}
	
	/**
	 * Pointer to the native structure
	 */
	private volatile long address = 0; // pointer to native structure
	
	/**
	 * @return the address pointer
	 */
	public long address() { return this.address; }
	
	public native long sec();
	public native long sec(long sec);
	
	public native long usec();
	public native long usec(long usec);
	
	public native int caplen();
	public native int len();
}
